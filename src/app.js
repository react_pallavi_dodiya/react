import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import AppRouters from "./playground/routers/AppRouters";
import configurestore from "./store/configureStore.js";
import { AddExpens } from "./actions/expenses";
import { setTextFilter } from "./actions/filters";
import getVisibleExpenses from "./selectors/expenses";
import "normalize.css/normalize.css";
import "./styles/styles.scss"

const store = configurestore();

store.dispatch(AddExpens({ discription: "water bill",amount:7000,creatAt:1000 }));
store.dispatch(AddExpens({ discription: "Gas bill",amount:400,creatAt:200 }));
store.dispatch(AddExpens({ discription: "coffee",amount:600,creatAt:3000 }));
// store.dispatch(setTextFilter("Gas"));
// setTimeout(()=>{store.dispatch(setTextFilter("bill"))},3000)
// store.dispatch(setStartDate(123));
// store.dispatch(setSortBy("date"));

const state = (store.getState());
const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
console.log(visibleExpenses);
const jsx =(
    <Provider store={store}>
        <AppRouters />
    </Provider>
)

ReactDOM.render( jsx, document.getElementById("root"))