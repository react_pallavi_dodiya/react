import moment from "moment";
export default (expenses,{text,sortBy,startDate,endDate})=>{
    return expenses.filter((expense)=>{
        const creatAtMoment =moment(expense.creatAt);
        const startDateMatch =  startDate ? startDate.isSameOrBefore(creatAtMoment,"day") :true; 
        const endDateMatch =  endDate ? endDate.isSameOrBefore(creatAtMoment,"day") :true; 
        const textMatch = expense.discription.toLowerCase().includes(text.toLowerCase());
        const filterValue =  startDateMatch && endDateMatch && textMatch
        return filterValue;
    }).sort((a,b)=>{
        if (sortBy === "date"){
            return a.creatAt < b.creatAt ? 1 :-1 
        }else if (sortBy === "amount") {
            return a.amount < b.amount ? 1 :-1
        }
    })
}