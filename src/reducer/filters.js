import moment from "moment";
//fitlers
const filterReducerDefaultState = {
    text: "",
    sortBy: "date",
    amount: 100,
    startDate:moment().startOf("month"),
    endDate: moment().endOf("month")
};

export default (state = filterReducerDefaultState, action) => {
    switch (action.type) {
        case "SET_TEXT_FILTER":
            return ({
                ...state,
                text: action.text
            });
        case "SORTBY_DATE":
            return ({
                ...state,
                sortBy: 'date'
            })
        case "SORTBY_AMOUNT":
            return ({
                ...state,
                sortBy: 'amount'
            })
        case "SET_START_DATE":
            return ({
                ...state,
                startDate: action.startdate
            })
        case "SET_END_DATE":
            return ({
                ...state,
                endDate: action.enddate
            })
        default:
            return state
    }
}