
//expenses
const expenseReducerDefaultState = [];
export default (state = expenseReducerDefaultState, action) => {
    switch (action.type) {
        case "ADD_EXPENSE":
            return [
                ...state,
                action.expense
            ];
        case "REMOVE_EXPENS":
            return state.filter(({ id }) => id !== action.id);
        case "EDIT_UPDATE":
            return state.map((expens) => {
                if (expens.id === action.id) {
                    return ({
                        ...expens,
                        ...action.update
                    });
                } else {
                    return expens
                }
            })
        default:
            return state;
    }
}