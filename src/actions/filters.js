
//set-text-filter
export const setTextFilter = (text = "") => ({
    type: "SET_TEXT_FILTER",
    text
})
//set state date
export const SortByDate = () => ({
    type: "SORTBY_DATE"
})
//sortBy amount
export const SortByAmount = () => ({
    type: "SORTBY_AMOUNT"
})

// set date
export const setStartDate = (startdate) => ({
    type: "SET_START_DATE",
    startdate
})
//end date
export const setEndDate = (enddate) => ({
    type: "SET_END_DATE",
    enddate
})