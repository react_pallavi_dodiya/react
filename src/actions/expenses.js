import uuid from "uuid";

//add_expens
export const AddExpens = (
    {
        discription = '',
        note = '',
        amount = 0,
        creatAt = 0
    } = {}
    ) => ({
    type: "ADD_EXPENSE",
    expense: {
        id: uuid(),
        discription,
        note,
        amount,
        creatAt
    }
});
//remove item
export const removeexpens = ({ id } = {}) => ({
    type: "REMOVE_EXPENS",
    id
})

//edit item
export const EditExpens = (id, update) => ({
    type: "EDIT_UPDATE",
    id,
    update
})
