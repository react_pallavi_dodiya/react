import {createStore,combineReducers} from "redux";
import expenseReducer from "../reducer/expenses";
import fitlerReducer from "../reducer/filters";

export default ()=>{
    const store = createStore(combineReducers({
        expenses: expenseReducer,
        filters: fitlerReducer
    }), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
 return store
}