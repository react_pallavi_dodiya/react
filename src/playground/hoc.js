import React from "react";
import ReactDOM from "react-dom";

// function header(){
//     console.log("Hello");

// }
// function footer(){
//     console.log("Bye");
// }

// function foo(a){
//     console.log("foo called:", a);
// }

// function help(){
//     console.log("help called\n");

// }

// function addHeaderFooter(func){
//     return (prop) => {
//         header();
//         func(prop);
//         footer();
//     }
// }

// let fooHeaderFooter = addHeaderFooter(foo);
// fooHeaderFooter(10);

const Info = (props)=>{
    return(
        <div>
            <h1>Info</h1>
            <p>Info:{props.info} </p>
        </div>
    )
}

const Requirement = (WrappedComponent) =>{
    return (props)=> (
        <div>
            {props.isAdmin && <p>hello world</p>}
            <WrappedComponent {...props}/>
        </div>
    );
};

const AddRequire = Requirement(Info);

// const requiAuthentication = (WrappedComponent)=>{
//     return (props)=> (
//         <div>
//             {props.isAuthentication ? (
//                 <p>you are in right way</p>
//             ) : (
//                 <p>view the details</p>
//             )}
//             <WrappedComponent {...props} />
//         </div>
//     )
// }
// const AuthInfo = requiAuthentication(Info)

ReactDOM.render(<AddRequire isAdmin = {false} info = "these are details" />, document.getElementById("root"))