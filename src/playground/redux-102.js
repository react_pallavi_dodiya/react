import { createStore } from "redux";

const store = createStore((state = { count: 1 }, action) => {
    switch (action.type) {
        case "INCREAMENT":
            return {
                count: state.count + 1
            }
        case "DECREAMENT":
            return {
                count: state.count - 1
            }
        case "RESET":
            return {
                count: 200
            }

        default:
            return state
    }
})

let un1= store.subscribe(()=>{
    console.log("Component1",store.getState())
});

let un2= store.subscribe(()=>{
    console.log("Component2",store.getState())
});

let un3= store.subscribe(()=>{
    console.log("Component3",store.getState())
});

store.dispatch({
    type: "INCREAMENT"
})
un2()
store.dispatch({
    type: "DECREAMENT"
})
store.dispatch({
    type: "RESET"
});