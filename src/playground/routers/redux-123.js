import { createStore, combineReducers } from "redux";
import uuid from "uuid"
//add_expens
const AddExpens = (
    {
        discription = '',
        note = '',
        amount = 0,
        creatAt = 0
    } = {}
    ) => ({
    type: "ADD_EXPENSE",
    expense: {
        id: uuid(),
        discription,
        note,
        amount,
        creatAt
    }
});
//remove item
const removeexpens = ({ id } = {}) => ({
    type: "REMOVE_EXPENS",
    id
})

//edit item
const EditExpens = (id, update) => ({
    type: "EDIT_UPDATE",
    id,
    update
})


//expenses
const expenseReducerDefaultState = [];
const expenseReducer = (state = expenseReducerDefaultState, action) => {
    switch (action.type) {
        case "ADD_EXPENSE":
            return [
                ...state,
                action.expense
            ];
        case "REMOVE_EXPENS":
            return state.filter(({ id }) => id !== action.id);
        case "EDIT_UPDATE":
            return state.map((expens) => {
                if (expens.id === action.id) {
                    return ({
                        ...expens,
                        ...action.update
                    });
                } else {
                    return expens
                }
            })
        default:
            return state;
    }
}
//set-text-filter
const setTextFilter = (text = "") => ({
    type: "SET_TEXT_FILTER",
    text
})
//set state date
const setSortBy = (sortBy = '') => ({
    type: "SET_SORTBY",
    sortBy
})
// set date
const setStartDate = (startdate) => ({
    type: "SET_START_DATE",
    startdate
})
//end date
const setEndDate = (enddate) => ({
    type: "SET_END_DATE",
    enddate
})






//fitlers
const filterReducerDefaultState = {
    text: "",
    sortBy: "date",
    amount: 100,
    startDate: undefined,
    endDate: undefined
}

const fitlerReducer = (state = filterReducerDefaultState, action) => {
    switch (action.type) {
        case "SET_TEXT_FILTER":
            return ({
                ...state,
                text: action.text
            });
        case "SET_SORTBY":
            return ({
                ...state,
                sortBy: action.sortBy
            })
        case "SET_START_DATE":
            return ({
                ...state,
                startDate: action.startdate
            })
        case "SET_END_DATE":
            return ({
                ...state,
                endDate: action.enddate
            })
        default:
            return state
    }
}
// filet use

const getVisibleExpense = (expenses,{text,sortBy,startDate,endDate})=>{
    return expenses.filter((expense)=>{
        const startDateMatch = typeof startDate !=="number" || expense.creatAt >= startDate; 
        const endDateMatch = typeof endDate !== "number" ||expense.creatAt <= endDate;
        const textMatch = expense.discription.toLowerCase().includes(text.toLowerCase());
        const filterValue =  startDateMatch && endDateMatch && textMatch
        return filterValue;
    }).sort((a,b)=>{
        if (sortBy === "date"){
            return a.creatAt < b.creatAt ? 1 :-1
        }else if (sortBy === "amount") {
            return a.amount < b.amount ? 1 :-1
        }
    })
}

//store
const store = createStore(combineReducers({
    expenses: expenseReducer,
    filters: fitlerReducer
}))
//subscribe
store.subscribe(() => {
   const state = store.getState()
   const visibleExpense = getVisibleExpense(state.expenses,state.filters);
   console.log(visibleExpense);
});
//dispatch
const expenseOne= store.dispatch(AddExpens({discription :"rent",amount:400, creatAt : 1000}));
const expenseTwo = store.dispatch(AddExpens({discription :"coffee",amount:300, creatAt : 1000}));
// store.dispatch(removeexpens({id : expenseOne.expense.id}));
// console.log("Edit ",store.dispatch(EditExpens (expenseTwo.expense.id,{amount : 600})));
// console.log("State:", store.getState())
 store.dispatch(setTextFilter("rent"));
// store.dispatch(setFilter());
// store.dispatch(setSortBy());
// store.dispatch(setSortBy(""));
// store.dispatch(setStartDate(123));
// store.dispatch(setEndDate(1200));
//demo state
const demoState = {
    expenses: [{
        id: "addehdifhsjf",
        discription: 'hey i am pallavi',
        note: "this was final payment",
        amount: 5000,
        creatAt: 0
    }],
    fitlers: {
        text: "rent",
        sortBy: 'date',
        startDate: undefined,
        endDate: undefined
    }
}