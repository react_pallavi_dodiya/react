import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
// import ReactDOM from "react-dom";
import Header from "../../components/Header";
import  Navbar from "../../components/Navbar";
import AddExpensePage from "../../components/AddExpenses";
import EditExpensePage from "../../components/EditExpensePage";
import ExpenseDashbordPage from "../../components/ExpenseDashbord";
import HelpPage from "../../components/HelpPage";
import NotFoundPage from "../../components/NotFound"



const AppRouters = () => {
    return (
        <BrowserRouter>
            <Header />
            <Navbar />
            <Switch>
                <Route path="/" component={ExpenseDashbordPage} exact={true}/>
                <Route exact={true} path="/edit/:id" component={EditExpensePage} />
                <Route path="/addexpense" component={AddExpensePage} />
                <Route path="/help" component={HelpPage}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </BrowserRouter>
    )
}

export default AppRouters;