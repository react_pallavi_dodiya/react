import React from "react";
import { NavLink } from "react-router-dom";
const Navbar = () => (
    <div className="navbar">
        <NavLink to="/" exact={true}> Go dashbord</NavLink>
        <NavLink className="nav-element" activeClassName="active-nav-element" to="/edit">EditExpense</NavLink>
        <NavLink className="nav-element" activeClassName="active-nav-element" to="/addexpense">AddExpense</NavLink>
        <NavLink className="nav-element" activeClassName="active-nav-element" to="/help">Help</NavLink>
    </div>
)
export default Navbar;