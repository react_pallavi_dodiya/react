import React from "react";
import { connect } from "react-redux";
// import { DateRangePicker } from "react-dates";
import { setTextFilter, SortByAmount, SortByDate, setStartDate, setEndDate } from "../actions/filters";
import { render } from "node-sass";

class ExpenseListFilter extends React.Component {
    state = {
        celenderFocused: null
    }
    onDatesChange = ({ startDate, endDate }) => {
        this.props.dispatch(setStartDate(startDate))
        this.props.dispatch(setEndDate(endDate))
    };
    onFocusChange = (celenderFocused) => {
        this.setState(() => ({ celenderFocused }));
    };
    render() {
        return (
            <div>
                <input type="text" value={props.filters.text} onChange={(e) => {
                    props.dispatch(setTextFilter(e.target.value))
                }} />
                <select value={props.filters.sortBy}
                    onChange={(e) => {
                        if (e.target.value === "date") {
                            props.dispatch(SortByDate());
                        } else if (e.target.value === "amount") {
                            props.dispatch(SortByAmount());
                        }
                    }}
                >
                    <option value="date" >Date</option>
                    <option value="amount">Amount</option>
                </select>
                {/* <DateRangePicker
                    startDate={this.props.filters.startDate}
                    endDate={this.props.filters.endtDate}
                    onDatesChange={this.onDatesChange}
                    focusedInput={this.state.celenderFocused}
                    onFocusChange={this.onFocusChange}
                    showClearDates={true}
                    numberOfMonths={1}
                    isOutsideRange={() => false}
                /> */}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        filters: state.filters
    }
}

export default connect(mapStateToProps)(ExpenseListFilter)