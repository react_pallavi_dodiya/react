import React from "react";
import { Link } from "react-router-dom";

const ExpenseListItem = ({  id, discription, amount, creatAt }) =>  (
        <div>
            <Link to={`/edit/${id}`}>
                <h3>{discription}</h3>
            </Link>
            <h4>{amount} - {creatAt}</h4>
        </div>
    )
export default ExpenseListItem;