import React from "react";
import moment from "moment";
import { SingleDatePicker } from "react-dates";
import "react-dates/lib/css/_datepicker.css";

export default class ExpenseForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            discription: props.expense ? props.expense.discription : '',
            note: props.expense ? props.expense.note : '',
            amount: props.expense ? (props.expense.amount).toString() : '',
            creatAt: props.expense ? moment(props.expense.creatAt) : moment(),
            focused: false,
            error: ""
        };
    }
    onDiscriptionChange = (e) => {
        const discription = e.target.value;
        this.setState(() => ({ discription }));
    };
    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState(() => ({ note }));
    }
    onAmountChange = (e) => {
        const amount = e.target.value;
        if (amount.match(/^\d*(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }))
        }
    }
    onDateChange = (creatAt) => {
        if (creatAt) {
            this.setState(() => ({ creatAt }));
        }
    }
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ celenderFocused: focused }));
    }
    onSubmit = (e) => {
        e.preventDefault();
        if (!this.state.discription || !this.state.amount) {
            this.setState(() => ({ error: "please provide discription and amount" }))
        } else {
            this.setState(() => ({ error: '' }))
            this.props.onSubmit({
                discription: this.state.discription,
                amount: parseFloat(this.state.amount, 10),
                creatAt: this.state.creatAt.valueOf(),
                note: this.state.note
            });
        }
    }
    render() {
        return (
            <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.onSubmit}>
                    <input
                        type="text"
                        placeholder="discription"
                        autoFocus
                        value={this.state.discription}
                        onChange={this.onDiscriptionChange}
                    />
                    <input
                        type="text"
                        placeholder="amount"
                        value={this.state.amount}
                        onChange={this.onAmountChange}
                    />
                    <SingleDatePicker
                        date={this.state.creatAt}
                        onDateChange={this.onDateChange}
                        focused={this.state.celenderFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                    <textarea
                        placeholder="add note for your expense(optional)"
                        value={this.state.note}
                        onChange={this.onNoteChange}
                    >
                    </textarea>
                    <button placeholder="addExpense">addExpense</button>
                </form>
            </div>
        )
    }
}